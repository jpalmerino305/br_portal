Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'main#index'

  get   'users/:role'           => 'users#index'
  get   'users/:role/new'       => 'users#new'
  post  'users/:role/new'       => 'users#create'
  get   'users/:role/edit/:id'  => 'users#edit'

  post  'users/validate-email-uniqueness'  => 'users#validate_email_uniqueness'

  resources :settings

  get 'purchases' => 'purchases#index'
  get 'purchases/:id' => 'purchases#show'

  get 'commissions' => 'commissions#index'
  get 'commissions/:id' => 'commissions#show'
  get 'commissions/edit/:user_id/:product_id' => 'commissions#edit'
  post 'commissions/update/:user_id/:product_id' => 'commissions#update'

  get 'customers/status/:status' => 'customers#index'
  get 'customers/request/:action_request' => 'customers#action_request'
  get 'customers/:id' => 'customers#show'
  post 'customers/assign_br/:id' => 'customers#assign_br'
  post 'customers/remove_br/:id' => 'customers#remove_br'

  namespace :api, defaults: { format: 'json' } do
    scope '1.0' do
      resources :purchases, only: [:create]
    end
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
