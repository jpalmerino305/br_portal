class CreateBrCommissions < ActiveRecord::Migration
  def change
    create_table :br_commissions do |t|
      t.integer "br_profile_id"
      t.string "product_id"
      t.string "introducer_commission"
      t.string "sales_person_commission"
      t.timestamps null: false
    end
  end
end
