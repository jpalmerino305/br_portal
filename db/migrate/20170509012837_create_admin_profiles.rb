class CreateAdminProfiles < ActiveRecord::Migration
  def change
    create_table :admin_profiles do |t|
      t.string  "first_name"
      t.string  "last_name"
      t.string  "email"
      t.date    "date_of_birth"
      t.string  "nric"
      t.string  "gender"
      t.timestamps null: false
    end
  end
end