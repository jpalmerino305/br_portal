class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer  "br_profile_id"
      t.string  "first_name"
      t.string  "last_name"
      t.string  "email"
      t.date    "date_of_birth"
      t.string  "nric"
      t.string  "gender"
      t.string  "request"
      t.string  "pruchase_status", defaule: "Unpurchased"
      t.string  "status", default: "New"
      t.timestamps null: false
    end
  end
end
