class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.integer  "customer_id"
      t.integer  "br_profile_id"
      t.string   "transaction_id"
      t.string   "product_name"
      t.string   "product_type"
      t.string   "product_code"
      t.string   "product_price"
      t.string   "commission_amount"
      t.string   "commissionable_amount"
      t.string   "portal_commission_percentage"
      t.string   "introducer_commission_percentage"
      t.string   "introducer_commission_amount"
      t.string   "sales_person_commission_percentage"
      t.string   "sales_person_commission_amount"
      t.string   "im_commission_percentage"
      t.string   "im_commission_amount"
      t.timestamps null: false
    end
  end
end
