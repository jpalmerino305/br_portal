# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170511064614) do

  create_table "admin_profiles", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.date     "date_of_birth"
    t.string   "nric",          limit: 255
    t.string   "gender",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "br_commissions", force: :cascade do |t|
    t.integer  "br_profile_id",           limit: 4
    t.string   "product_id",              limit: 255
    t.string   "introducer_commission",   limit: 255
    t.string   "sales_person_commission", limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "br_profiles", force: :cascade do |t|
    t.string   "code",          limit: 255
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.date     "date_of_birth"
    t.string   "nric",          limit: 255
    t.string   "gender",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "customers", force: :cascade do |t|
    t.integer  "br_profile_id",   limit: 4
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.string   "email",           limit: 255
    t.date     "date_of_birth"
    t.string   "nric",            limit: 255
    t.string   "gender",          limit: 255
    t.string   "request",         limit: 255
    t.string   "pruchase_status", limit: 255
    t.string   "status",          limit: 255, default: "New"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  create_table "hos_profiles", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.date     "date_of_birth"
    t.string   "nric",          limit: 255
    t.string   "gender",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "hr_profiles", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.date     "date_of_birth"
    t.string   "nric",          limit: 255
    t.string   "gender",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "code",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "purchases", force: :cascade do |t|
    t.integer  "customer_id",                        limit: 4
    t.integer  "br_profile_id",                      limit: 4
    t.string   "transaction_id",                     limit: 255
    t.string   "product_name",                       limit: 255
    t.string   "product_type",                       limit: 255
    t.string   "product_code",                       limit: 255
    t.string   "product_price",                      limit: 255
    t.string   "commission_amount",                  limit: 255
    t.string   "commissionable_amount",              limit: 255
    t.string   "portal_commission_percentage",       limit: 255
    t.string   "introducer_commission_percentage",   limit: 255
    t.string   "introducer_commission_amount",       limit: 255
    t.string   "sales_person_commission_percentage", limit: 255
    t.string   "sales_person_commission_amount",     limit: 255
    t.string   "im_commission_percentage",           limit: 255
    t.string   "im_commission_amount",               limit: 255
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "value",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "loginable_id",           limit: 4
    t.string   "loginable_type",         limit: 255
    t.string   "role",                   limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
