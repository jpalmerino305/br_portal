# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Sample Admin User
user = User.create(email: "john.doe@mail.com", password: "1234567890", role: "admin")
profile = AdminProfile.create(first_name: "John", last_name: "Doe", email: "john.doe@mail.com", date_of_birth: "1986-07-27", nric: "1234", gender: "Male")
profile.user = user
profile.save

# Head of Sales User
user = User.create(email: "marne@mail.com", password: "1234567890", role: "hos")
profile = HosProfile.create(first_name: "Marne", last_name: "Rohlf", email: "marne@mail.com", date_of_birth: "1977-03-12", nric: "1234", gender: "Female")
profile.user = user
profile.save

# Human Resource User
user = User.create(email: "lily@mail.com", password: "1234567890", role: "hr")
profile = HrProfile.create(first_name: "Lily", last_name: "Adlington", email: "lily@mail.com", date_of_birth: "1944-03-16", nric: "1234", gender: "Female")
profile.user = user
profile.save

# Broker Representative User
user = User.create(email: "bernardo@mail.com", password: "1234567890", role: "br")
br_profile1 = BrProfile.create(first_name: "Bernardo", last_name: "Catley", email: "bernardo@mail.com", date_of_birth: "1984-10-06", nric: "1234", gender: "Male")
br_profile1.user = user
br_profile1.save

user = User.create(email: "ebarkleyl@163.com", password: "1234567890", role: "br")
br_profile2 = BrProfile.create(first_name: "Elvis", last_name: "Barkley", email: "ebarkleyl@163.com", date_of_birth: "1986-02-28", nric: "1234", gender: "Male")
br_profile2.user = user
br_profile2.save

user = User.create(email: "aclemettsq@taobao.com", password: "1234567890", role: "br")
br_profile3 = BrProfile.create(first_name: "Antonina", last_name: "Clemetts", email: "aclemettsq@taobao.com", date_of_birth: "1982-08-09", nric: "1234", gender: "Female")
br_profile3.user = user
br_profile3.save


# Sample new customers

Customer.create([
  { first_name: "Raymund", last_name: "Laird-Craig", email: "rlairdcraig0@icq.com", date_of_birth: "1983-10-20", nric: "00010", gender: "Male" },
  { first_name: "Tess", last_name: "Toffano", email: "ttoffano1@oracle.com", date_of_birth: "1990-08-08", nric: "00011", gender: "Female" },
  { first_name: "Brien", last_name: "Sexten", email: "bsexten2@google.de", date_of_birth: "1984-05-11", nric: "00012", gender: "Male" },
  { first_name: "Kevina", last_name: "Lathan", email: "klathan3@jiathis.com", date_of_birth: "1984-12-03", nric: "00013", gender: "Female" },
  { first_name: "Rickie", last_name: "Rudiger", email: "rgreensides4@ustream.tv", date_of_birth: "1982-12-13", nric: "00014", gender: "Female" },
  { first_name: "Antoinette", last_name: "Cesaric", email: "arudiger5@printfriendly.com", date_of_birth: "1964-06-06", nric: "00015", gender: "Female" },
  { first_name: "Genna", last_name: "Trotton", email: "gcesaric6@eventbrite.com", date_of_birth: "1959-04-03", nric: "00016", gender: "Female" },
  { first_name: "Coleen", last_name: "Bromfield", email: "ctrotton7@phoca.cz", date_of_birth: "1908-05-13", nric: "00017", gender: "Female" },
  { first_name: "Xever", last_name: "Greensides", email: "xbromfield8@bravesites.com", date_of_birth: "1958-03-27", nric: "00018", gender: "Male" },
  { first_name: "Hilario", last_name: "O'Toole", email: "hotoole9@bloomberg.com", date_of_birth: "1954-08-18", nric: "00019", gender: "Male" }
])

# Sample existing customers

Customer.create([
  { br_profile_id: br_profile1.id, first_name: "Jenn", last_name: "Utridge", email: "jutridged@reference.com", date_of_birth: "1952-06-16", nric: "00001", gender: "Female", request: "Change", status: "Existing" },
  { br_profile_id: br_profile1.id, first_name: "Ewart", last_name: "Chaudhry", email: "echaudhrye@gmpg.org", date_of_birth: "1975-05-26", nric: "00002", gender: "Male", request: "Change" },
  { br_profile_id: br_profile1.id, first_name: "Boris", last_name: "Cussons", email: "bcussonsf@cargocollective.com", date_of_birth: "1974-07-14", nric: "00003", gender: "Male", request: "Change", status: "Existing" },
  { br_profile_id: br_profile2.id, first_name: "Karine", last_name: "Willmett", email: "kwillmettg@drupal.org", date_of_birth: "1965-12-25", nric: "00004", gender: "Female", request: "Change", status: "Existing" },
  { br_profile_id: br_profile2.id, first_name: "Leonanie", last_name: "Rushmare", email: "lrushmareh@artisteer.com", date_of_birth: "1957-12-11", nric: "00005", gender: "Female", request: "Change", status: "Existing" },
  { br_profile_id: br_profile2.id, first_name: "Jean", last_name: "Sarney", email: "jsarneyi@rambler.ru", date_of_birth: "1964-06-06", nric: "00000", gender: "Female", request: "Change", status: "Existing" },
  { br_profile_id: br_profile2.id, first_name: "Diannne", last_name: "Tremolieres", email: "dtremolieresj@spiegel.de", date_of_birth: "1959-04-03", nric: "00006", gender: "Female", request: "Opt out", status: "Existing" },
  { br_profile_id: br_profile3.id, first_name: "Meagan", last_name: "Gratrex", email: "mgratrexk@cornell.edu", date_of_birth: "1908-05-13", nric: "00007", gender: "Female", request: "Opt out", status: "Existing" },
  { br_profile_id: br_profile3.id, first_name: "Terence", last_name: "Filinkov", email: "tfilinkovl@bigcartel.com", date_of_birth: "1958-03-27", nric: "00008", gender: "Male", request: "Opt out", status: "Existing" },
  { br_profile_id: br_profile3.id, first_name: "Sampson", last_name: "Mathonnet", email: "smathonnetm@salon.com", date_of_birth: "1954-08-18", nric: "00009", gender: "Male", request: "Opt out", status: "Existing" }
])

# Products

Product.create([
  { code: "P001", name: "Travel" },
  { code: "P002", name: "Home" },
  { code: "P003", name: "Maid" },
  { code: "P004", name: "Personal" },
  { code: "P005", name: "Pet" },
  { code: "P006", name: "Car" }
])

# Commission Settings
Setting.create(name: "Commission", value: "50")