class SettingsController < ApplicationController

  before_filter :authenticate_user!

  def index
    @settings = Setting.all
    @crumbs = [{ name: "Settings", url: "", active: true}]
  end
  
  def edit
    @setting = Setting.find(params[:id])
    @crumbs = [{ name: "Settings", url: url_for(action: "index"), active: false}, { name: @setting.name, url: "", active: true}]
  end
  
  def update
    @setting = Setting.find(params[:id])
    if @setting.update(settings_params)
      flash[:notice] = "Setting successfully updated"
      redirect_to action: "index"
    else
      render action: "edit"
    end
  end


  def settings_params
    params.require(:setting).permit(:name, :value)
  end

end
