class CommissionsController < ApplicationController

  before_filter :authenticate_user!

  def index
    @users = BrProfile.all
    @crumbs = [{ name: "Commissions", url: "", active: true}]
  end

  def show
    @products = Product.all
    @user = BrProfile.find(params[:id])
    @user_commissions = @user.br_commissions
    @crumbs = [{ name: "Commissions", url: url_for(action: "index"), active: false}, { name: "#{@user.first_name} #{@user.last_name}", url: "", active: true}]
  rescue ActiveRecord::RecordNotFound
    flash[:warning] = "Couldn't find Commission with id #{params[:id]}"
    redirect_to :action => 'index'
  end

  def edit
    @user = BrProfile.find(params[:user_id])
    @product = Product.find(params[:product_id])

    @br_commission = BrCommission.where(br_profile_id: @user.id, product_id: @product.id).first
    if !@br_commission
      @br_commission = BrCommission.new
    end

    url = url_for action: "index"
    show_url = url_for action: "show", id: @user.id
    @crumbs = [
      { name: "Commissions", url: url, active: false}, 
      { name: "#{@user.first_name} #{@user.last_name}", url: show_url, active: false}, 
      { name: "Settings", url: "", active: true}
    ]
  rescue ActiveRecord::RecordNotFound
    flash[:warning] = "Couldn't find BR or Product"
    redirect_to :action => 'index'
  end

  def update
    commission = BrCommission.where(br_profile_id: params[:user_id], product_id: params[:product_id]).first
    if commission
      commission.introducer_commission = params[:introducer_commission]
      commission.sales_person_commission = params[:sales_person_commission]
      if commission.save
        flash[:notice] = "Successfully updated commission settings"
      else
        flash[:notice] = "Updating commission settings failed"
      end
    else
      commission = BrCommission.new
      commission.br_profile_id = params[:user_id]
      commission.product_id = params[:product_id]
      commission.introducer_commission = params[:introducer_commission]
      commission.sales_person_commission = params[:sales_person_commission]
      if commission.save
        flash[:notice] = "Successfully created commission settings"
      else
        flash[:notice] = "Updating commission settings failed"
      end
    end
    render json: { commission: commission, redirect_url: url_for(action: "show", id: params[:user_id]) }
  end

end
