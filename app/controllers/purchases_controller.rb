class PurchasesController < ApplicationController

  before_filter :authenticate_user!

  def index
    per_page = 10
    @purchases = Purchase.paginate(per_page: per_page, :page => params[:page])
    @crumbs = [{ name: "Purchases", url: "", active: true}]
  end

  def show
    @purchase = Purchase.find(params[:id])
    @crumbs = [{ name: "Purchases", url: url_for(action: "index"), active: false}, { name: "Details", url: "", active: true}]
  rescue ActiveRecord::RecordNotFound
    flash[:warning] = "Couldn't find Purchase with id #{params[:id]}"
    redirect_to :action => 'index'
  end

end
