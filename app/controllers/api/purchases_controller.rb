class Api::PurchasesController < ApplicationController

  include CommissionsHelper

  protect_from_forgery except: :create

  def create

    response = {}
    errors = []
    status = 0

    purchase = Purchase.none
    br_profile = BrProfile.where(code: params[:br_code]).first
    product = Product.where(code: params[:product_code]).first

    if !br_profile
      errors.push("Invalid BR code")
    end
    if !product
      errors.push("Invalid product code")
    end

    if errors.length == 0
      commission = BrCommission.where(br_profile_id: br_profile.id, product_id: product.id).first

      # Variable declerations for portal commission and commissionable amount
      portal_commission = 0.0
      commissionable_amount = 0.0
      commission_setting = Setting.get('Commission')
      if commission_setting
        portal_commission = commission_setting.value
        commissionable_amount = (commission_setting.value.to_f / 100) * params[:commission].to_f
      end

      # Introducer Commission
      introducer_commission_percentage = 0.00
      introducer_commission_amount = 0.00

      # Sales Person Commission
      sales_person_commission_percentage = 0.00
      sales_person_commission_amount = 0.00
      
      # IM Commission
      im_commission_percentage = 0.00
      im_commission_amount = 0.00


      if commission
        response[:status] = "Commission found"

        introducer_commission_percentage = commission.introducer_commission.to_f
        sales_person_commission_percentage = commission.sales_person_commission.to_f

        # Check if there is a commission for Introducer or Sales Person
        has_introducer = true
        if introducer_commission_percentage <= 0.00
          has_introducer = false
        end
        has_sales_person =true
        if sales_person_commission_percentage <= 0.00
          has_sales_person = false
        end

        im_commission_percentage = 0.00
        im_commission_amount = 0.00

        introducer_commission_amount = 0.00
        sales_person_commission_amount = 0.00

        # Condition for Introducer, Sales Person and IM commission
        if has_introducer && has_sales_person
          response[:note] = "Introducer and Sales Person has commission"
          introducer_commission_amount = commission_amount(introducer_commission_percentage, commissionable_amount)
          sales_person_commission_amount = commission_amount(sales_person_commission_percentage, commissionable_amount)

        elsif has_introducer
          response[:note] = "Only Introducer has commission"
          introducer_commission_amount = commission_amount(introducer_commission_percentage, commissionable_amount)

          im_commission_percentage = 100.to_f - introducer_commission_percentage
          im_commission_amount = commission_amount(im_commission_percentage, commissionable_amount)

        elsif has_sales_person
          response[:note] = "Only Sales Person has commission"
          sales_person_commission_amount = commission_amount(sales_person_commission_percentage, commissionable_amount)

          im_commission_percentage = 100.to_f - sales_person_commission_percentage
          im_commission_amount = commission_amount(im_commission_percentage, commissionable_amount)

        else
          response[:note] = "Introducer and Sales Person has no commission"
          im_commission_percentage = 100.to_f
          im_commission_amount = commission_amount(im_commission_percentage, commissionable_amount)
        end

        response[:portal_commission] = portal_commission
        response[:commissionable_amount] = commissionable_amount

        response[:introducer_commission_percentage] = introducer_commission_percentage
        response[:introducer_commission_amount] = introducer_commission_amount

        response[:sales_person_commission_percentage] = sales_person_commission_percentage
        response[:sales_person_commission_amount] = sales_person_commission_amount

        response[:im_commission_percentage] = im_commission_percentage
        response[:im_commission_amount] = im_commission_amount

        response[:im_commission_percentage] = im_commission_percentage
        response[:im_commission_amount] = im_commission_amount
    
      else

        # Introducer and Sales Person don't have commission
        response[:status] = "Couldn't find Commission"

        portal_commission = 0.0
        commissionable_amount = 0.0
        commission_setting = Setting.get('Commission')
        if commission_setting
          portal_commission = commission_setting.value
          commissionable_amount = (commission_setting.value.to_f / 100) * params[:commission].to_f
        end

        im_commission_percentage = 100.to_f
        im_commission_amount = commission_amount(im_commission_percentage, commissionable_amount)

        response[:im_commission_percentage] = im_commission_percentage
        response[:im_commission_amount] = im_commission_amount

      end

      # Save purchase
      purchase = Purchase.new
      purchase.customer_id = params[:user_id]
      purchase.br_profile_id = br_profile.id
      purchase.transaction_id = params[:transaction_id]
      purchase.product_name = params[:product_name]
      purchase.product_type = params[:product_type]
      purchase.product_code = params[:product_code]
      purchase.product_price = params[:product_price]
      purchase.commission_amount = params[:commission]
      purchase.commissionable_amount = commissionable_amount
      purchase.portal_commission_percentage = portal_commission

      # Introducer Commission
      purchase.introducer_commission_percentage = introducer_commission_percentage
      purchase.introducer_commission_amount = introducer_commission_amount

      # Sales Person Commission
      purchase.sales_person_commission_percentage = sales_person_commission_percentage
      purchase.sales_person_commission_amount = sales_person_commission_amount
      
      # IM Commission
      if im_commission_percentage > 0.00
        purchase.im_commission_percentage = im_commission_percentage
        purchase.im_commission_amount = im_commission_amount
      end

      if purchase.save
        status = 1
      else
        status = 0
      end
    end

    # response[:params] = params
    render json: { errors: errors, purchase: purchase, status: status }
    # render json: { errors: errors, purchase: purchase, status: status, response: response } # Added response for debugging purposes
  end

end
