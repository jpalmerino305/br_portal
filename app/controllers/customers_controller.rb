class CustomersController < ApplicationController

  before_filter :authenticate_user!

  def index
    per_page = 10
    case params[:status]
      when "new"
        @customers = Customer.where(status: "New").paginate(per_page: per_page, :page => params[:page])
        @title = "New customers"
      when "existing"
        @customers = Customer.where(status: "Existing").paginate(per_page: per_page, :page => params[:page])
        @title = "Existing customers"
    end
    @crumbs = [{ name: @title, url: "", active: true}]
  end

  def action_request
    per_page = 10
    case params[:action_request]
      when "change"
        @customers = Customer.where(request: "Change").paginate(per_page: per_page, :page => params[:page])
        @title = "Customers change BR Request"
      when "opt-out"
        @customers = Customer.where(request: "Opt out").paginate(per_page: per_page, :page => params[:page])
        @title = "Customers Opt out Request"
    end
    @crumbs = [{ name: @title, url: "", active: true}]
  end

  def show
    @br_profiles = BrProfile.all
    @customer = Customer.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:warning] = "Couldn't find Customer with id #{params[:id]}"
    redirect_to url_for action: "index", status: "new"
  end

  def assign_br
    customer = Customer.find(params[:id])
    if customer
      customer.br_profile_id = params[:br_profile_id]
      customer.status = "Existing"
      if customer.save
        flash[:notice] = "Broker Representative successfully assigned to customer"
      end
    end
    render json: { customer: customer, br_profile: customer.br_profile}
  end

  def remove_br
    customer = Customer.find(params[:id])
    if customer
      customer.br_profile_id = nil
      customer.request = ""
      if customer.save
        flash[:notice] = "Broker Representative successfully removed to customer"
      end
    end
    render json: { customer: customer, br_profile: customer.br_profile}
  end

end
