class UsersController < ApplicationController

  before_filter :authenticate_user!

  # include ApplicationHelper
  # include Rails.application.routes.url_helpers

  def index
    check_role_param(params['role'])
    case params[:role]
      when "admin"
        @users = AdminProfile.all
        @title = "Admin Users List"
      when "head-of-sales"
        @users = HosProfile.all
        @title = "Head of Sales Users List"
      when "human-resources"
        @users = HrProfile.all
        @title = "HR Users List"
      when "broker-representative"
        @users = BrProfile.all
        @title = "Broker Representative Users List"
    end

    url = url_for controller: "users", action: "new", role: params[:role]
    @crumbs = [{ name: @title, url: url, active: true}]


  end

  def new
    check_role_param(params['role'])

    @marital_statuses = [
      ["Single", "Single"],
      ["Married", "Married"],
      ["Divorced", "Divorced"],
      ["Widdowed", "Widdowed"]
    ]

    case params[:role]
      when "admin"
        @user = AdminProfile.new
        @title = "New Admin user"
        @prev_title = "Admin Users List"
        @role = "admin"
      when "head-of-sales"
        @user = HosProfile.new
        @title = "New Head of Sales user"
        @prev_title = "Head of Sales List"
        @role = "hos"
      when "human-resources"
        @user = HrProfile.new
        @title = "New Human Resources user"
        @prev_title = "Human Resources List"
        @role = "hr"
      when "broker-representative"
        @user = BrProfile.new
        @title = "New Broker Representative user"
        @prev_title = "Broker Representative List"
        @role = "br"
    end

    url = url_for controller: "users", action: "index", role: params[:role]
    @crumbs = [{ name: @prev_title, url: url, active: false}, { name: @title, url: "", active: true}]

  end

  def create
    check_role_param(params['role'])
    user_role = params['user_role']

    case user_role
      when "admin"
        @user = AdminProfile.new(user_params)
        success_notice = "Successfully created Admin user!"
      when "hos"
        @user = HosProfile.new(user_params)
        success_notice = "Successfully created Head of Sales user!"
      when "hr"
        @user = HrProfile.new(user_params)
        success_notice = "Successfully created Human Reseources user!"
      when "br"
        @user = BrProfile.new(user_params)
        success_notice = "Successfully created Broker Representative user!"
    end

    if @user.save

      login = User.new
      login.email = user_params[:email]
      login.password = params[:password]
      login.role = user_role
      login.save

      login.loginable = @user
      login.save

      flash[:notice] = success_notice

      status = 1
    else

      status = 0
    end

    render json: { status: status, user: @user }
  end

  def edit
    check_role_param(params['role'])
  
    case params[:role]
      when "admin"
        @user = AdminProfile.find(params[:id])
        @title = "Edit Admin user"
        @prev_title = "Admin Users List"
        @role = "admin"
      when "head-of-sales"
        @user = HosProfile.find(params[:id])
        @title = "Edit Head of Sales user"
        @prev_title = "Head of Sales List"
        @role = "hos"
      when "human-resources"
        @user = HrProfile.find(params[:id])
        @title = "Edit Human Resources user"
        @prev_title = "Human Resources List"
        @role = "hr"
      when "broker-representative"
        @user = BrProfile.find(params[:id])
        @title = "Edit Broker Representative user"
        @prev_title = "Broker Representative List"
        @role = "br"
    end

    url = url_for controller: "users", action: "index", role: params[:role]
    @crumbs = [{ name: @prev_title, url: url, active: false}, { name: @title, url: "", active: true}]
  end

  def update
    @user = HosProfile.find(params[:id])
  end

  def validate_email_uniqueness
    errors = []
    user = User.where(["email = ?", params[:email]])
    if user.count > 0
      errors.push('Email already exist')
    end
    render json: { errors: errors }
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :date_of_birth, :email, :nric, :gender)
  end

  def check_role_param(role)
    # valid_role_param = ["head-of-sales", "hr"]
    # if !valid_role_param.include?(role)
    #   redirect_to controller: "/admin", action: "index"
    # end
  end

end