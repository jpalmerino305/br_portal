module CommissionsHelper

  def commission_amount(percent, amount)
    (percent / 100) * amount
  end

end
