module ApplicationHelper

  def is_nav_active(controllers, actions)
    active = false
    if controllers.include? params[:controller]
      if actions.include?("*") || actions.include?(params[:action])
        active = true
      end
    end
    return active
  end

  def show_crumbs(arr)
    str = ""
    if arr && arr.length > 0
      str += "<ul class=\"breadcrumb\">"
      str += "<li><a href=\"/\">Home</a></li>"
      arr.each do |a|
        if a[:active]
          str += "<li class=\"active\">#{a[:name]}</li>"
        else
          str += "<li><a href=\"#{a[:url]}\">#{a[:name]}</a></li>"
        end
      end
      str += "</ul>"
    end
    return str.html_safe
  end

end
