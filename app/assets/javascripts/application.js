// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require jquery-ui/widgets/datepicker
//= require jquery.number.min

//= require bootstrap-dialog.min

//= require_tree .

$(document).ready(function(){


  $('.number').number( true, 2 );

  var min_age = 20;
  var max_age = 90;

  $('.datepicker').datepicker({
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '-' + (max_age + 1) + ':-' + (min_age + 1),
    defaultDate: '-' + (max_age + 1) + 'y'
    // buttonImage: 'contact/calendar/calendar.gif',
    // buttonImageOnly: true,
  });


  function display_errors(errors, element){
    var str = '';
    str += '<div class="alert alert-danger alert-dismissable">';
    str += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';

    if (errors.length > 0){
      for (var i = 0; i < errors.length; i ++){
        str += '<div><i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;' + errors[i] + '</div>';
      }
    }

    str += '</div>';

    if (errors.length > 0){
      element.html(str).show();
    }
  }


  $('#user-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();

    setTimeout(function(){}, 1000);

    $('input[name="user[gender]"]').closest('.form-group').addClass('has-error');

    var form = $(this);
    var authenticity_token = $('input[name="authenticity_token"]');
    var user_role = $('input[name="user_role"]');
    var first_name = $('input[name="user[first_name]"]');
    var last_name = $('input[name="user[last_name]"]');
    var date_of_birth = $('input[name="user[date_of_birth]"]');
    var email = $('input[name="user[email]"]');
    var nric = $('input[name="user[nric]"]');
    var gender = $('input[name="user[gender]"]');
    var password = $('input[name="password"]');

    var errors = validateForm();
    var html_errors = '';

    if (errors.length > 0){
      display_errors(errors, $('#form-errors'))
    } else {
      console.log('Valid form....');

      var dob_arr = date_of_birth.val().split('/');

      var form_data = {
        authenticity_token: authenticity_token.val(),
        user_role: user_role.val(),
        user:{
          first_name: first_name.val(),
          last_name: last_name.val(),
          date_of_birth: dob_arr[2] + '-' + dob_arr[1] + '-' + dob_arr[0],
          email: email.val(),
          nric: nric.val(),
          gender: $('input[name="user[gender]"]:checked').val()
        },
        password: password.val()
      }
      console.log(form_data);
      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: form_data,
        success: function(result){
          if (result.status == 1){
            window.location.href = form.attr('redirect-url');
          }
        },
        error: function(result){
        }
      });

    }

    return false;
  });

  function validateForm(){

    var first_name = $('input[name="user[first_name]"]');
    var last_name = $('input[name="user[last_name]"]');
    var date_of_birth = $('input[name="user[date_of_birth]"]');
    var email = $('input[name="user[email]"]');
    var nric = $('input[name="user[nric]"]');
    var gender = $('input[name="user[gender]"]');
    var password = $('input[name="password"]');
    var password_confirmation = $('input[name="password_confirmation"]');

    var errors = [];

    if (first_name.val() == ''){
      errors.push('First name can\'t be blank');
    }

    if (last_name.val() == ''){
      errors.push('Last name can\'t be blank');
    }

    if (date_of_birth.val() == ''){
      errors.push('Date of birth can\'t be blank');
    } else {
      var age = getAge(date_of_birth.val());
      if (age < min_age || age > max_age){
        errors.push('Age must be between ' + min_age + ' to ' + max_age + ' years old');
      }
      console.log('age: ' + age);
    }

    if (email.val() == ''){
      errors.push('Email can\'t be blank');
    } else {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.val())){  
        var valid_email = validateEmailUniqueness(email.val());
        if (valid_email.length > 0){
          for (var i = 0; i < valid_email.length; i ++){
            errors.push (valid_email[i]);
          }
        }
      } else {
        errors.push('Invalid email format');
      }
    }

    if (nric.val() == ''){
      errors.push('NRIC/FIN can\'t be blank');
    }

    if (!$('input[name="user[gender]"]:checked').val()){
      errors.push('Please select a gender');
    }

    var empty_pass = false;
    if (password.val() == ''){
      empty_pass = true;
      errors.push('Password can\'t be blank');
    }
    if (password_confirmation.val() == ''){
      empty_pass = true;
      errors.push('Password confirmation can\'t be blank');
    }
    if (!empty_pass){
      if (password.val() != password_confirmation.val()){
        errors.push('Password confirmation don\'t match');
      } else {
        var min_pass_length = 6;
        if (password.val().length < min_pass_length){
          errors.push('Password lenght must be at least ' + min_pass_length + ' characters');
        }
      }
    }

    return errors;

  }

  function getAge(dob){
    var str = dob.split('/');
    var firstdate = new Date(str[2],str[1],str[0]);
    var today = new Date();        
    var dayDiff = Math.ceil(today.getTime() - firstdate.getTime()) / (1000 * 60 * 60 * 24 * 365);
    var age = parseInt(dayDiff);
    return age;
  }

  function validateEmailUniqueness(email){
    var res = [];
    $.ajax({
      type: 'POST',
      async: false,
      url: '/users/validate-email-uniqueness',
      data: {
        email: email
      },
      beforeSend: function(xhr){
        // $('#loader').show();
      },
      success: function(result){
        console.log('success -> result', result);
        res = result.errors;
        // $('#loader').hide();
      },
      error: function(result){
      }
    });
    return res;
  }

  $('#assign-br').on('submit', function(e){
    e.preventDefault();
    var form = $(this);
    var br_profile_id = $('select[name=br_profile_id]').val();
    var token = $('meta[name="csrf-token"]').attr('content');

    console.log('br_profile_id: ' + br_profile_id);
    console.log('token: ' + token);


    $.ajax({
      type: 'POST',
      async: false,
      url: form.attr('action'),
      data: {
        authenticity_token: token,
        br_profile_id: br_profile_id
      },
      beforeSend: function(xhr){
        // $('#loader').show();
      },
      success: function(result){
        console.log(result);
        location.reload();
      },
      error: function(result){
      }
    });


  });


  $('#remove-br').on('submit', function(e){
    e.preventDefault();
    var form = $(this);
    var token = $('meta[name="csrf-token"]').attr('content');

    if (confirm('Are you sure?')){
      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: {
          authenticity_token: token
        },
        beforeSend: function(xhr){
          // $('#loader').show();
        },
        success: function(result){
          console.log(result);
          location.reload();
        },
        error: function(result){
        }
      });
    }

  });

  $('#commission-form').on('submit', function(e){
    e.preventDefault();
    $('#form-errors').html('').hide();

    var form = $(this);
    var errors = [];
    var authenticity_token = $('input[name="authenticity_token"]');
    var introducer_commission = $('input[name="br_commission[introducer_commission]"]');
    var sales_person_commission = $('input[name="br_commission[sales_person_commission]"]');
    var commissions = { introducer: 0.0, sales_person: 0.0 };

    var has_introducer = true;
    if (parseFloat(introducer_commission.val()) == 0.0 || introducer_commission.val() == ''){
      var has_introducer = false;
    }
    var has_sales_person = true;
    if (parseFloat(sales_person_commission.val()) == 0.0 || sales_person_commission.val() == ''){
      var has_sales_person = false;
    }

    if (has_introducer && has_sales_person){
      // console.log('Has introducer and has sales person');
      commissions.introducer = parseFloat(introducer_commission.val());
      commissions.sales_person = parseFloat(sales_person_commission.val());
      var total_commission = parseFloat(introducer_commission.val()) + parseFloat(sales_person_commission.val());
      if (total_commission != 100){
        errors.push('Introducer and Sales Person Commission must have a sum of 100%');
      }

    } else if (has_introducer){
      // console.log('Has introducer and no sales person');
      commissions.introducer = parseFloat(introducer_commission.val());
      if (parseFloat(introducer_commission.val()) > 100){
        errors.push('Introducer commission must not be greater than 100%');
      }

    } else if (has_sales_person){
      // console.log('Has sales person and no introducer');
      commissions.sales_person = parseFloat(sales_person_commission.val());
      if (parseFloat(sales_person_commission.val()) > 100){
        errors.push('Sales Person commission must not be greater than 100%');
      }

    }

    console.log(commissions);

    if (errors.length > 0){
      display_errors(errors, $('#form-errors'))
    } else {
      $.ajax({
        type: 'POST',
        async: false,
        url: form.attr('action'),
        data: {
          authenticity_token: authenticity_token.val(),
          introducer_commission: commissions.introducer,
          sales_person_commission: commissions.sales_person
        },
        beforeSend: function(xhr){
          // $('#loader').show();
        },
        success: function(result){
          console.log(result);
          window.location = result.redirect_url;
          // location.reload();
        },
        error: function(result){
        }
      });
    }

  });

});