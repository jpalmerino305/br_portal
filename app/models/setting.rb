class Setting < ActiveRecord::Base
  validates :name, uniqueness: true
  validates :value, presence: true

  def self.get(name)
    Setting.where(name: name).first
  end
end