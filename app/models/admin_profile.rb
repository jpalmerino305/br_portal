class AdminProfile < ActiveRecord::Base
  has_one :user, as: :loginable

  belongs_to :br_profile
end
