class BrProfile < ActiveRecord::Base

  has_one :user, as: :loginable

  has_many :br_commissions
  has_many :customers
  has_many :purchases

  before_create :generate_code

  validates :email, uniqueness: true

  private

  def generate_code
    random_code = ""
    range = [*'0'..'9',*'A'..'Z']
    unique = false
    while !unique
      random_alphanum = Array.new(6){ range.sample }.join
      random_code = Time.now.strftime("%m").to_s + random_alphanum
      users = BrProfile.where(code: random_code)
      if users.count == 0
        unique = true
      end
    end
    self.code = random_code
  end

end
