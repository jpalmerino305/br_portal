class Customer < ActiveRecord::Base
  belongs_to :br_profile

  has_many :purchases

  validates :email, uniqueness: true
end
